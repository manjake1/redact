import os
import re
import csv
import logging
import yaml
import argparse
import itertools

REDACT_REPLACE_WITH = " REDACTED "
CSV_FILE = "import.csv"
YAML_FILE = "import.yaml"

def arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--path', type=str, required=True, help=r"Enter path to working dir e.g D:\temp\redact")
    parsed_args = parser.parse_args()
    return parsed_args


def main(base_dir):
    logging.info("Script started")
    os.chdir(base_dir)
    file_extensions = (".htm", ".html", ".HTM", ".HTML")
    html_files = [f for f in os.listdir(base_dir) if f.endswith(file_extensions)]
    redact_set = import_csv()
    redact_set_length = len(redact_set)
    logging.info(f"Matching {redact_set_length} words")
    logging.info(redact_set)
    regex_dict = import_yaml()
    regex_dict_length = len(regex_dict)
    logging.info(f"Matching {regex_dict_length} regex")
    logging.info(regex_dict)

    for file in html_files:
        for regex_key, regex_val in regex_dict.items():
            try:
                redact_regex(file, regex_val)
            except Exception as e:
                logging.error(f"{file} {regex_key} regex redaction failed {e}")
            else:
                logging.info(f"{file} {regex_key} regex redaction successful")
        for string_to_redact in redact_set:
            try:
                redact_string(file, string_to_redact.lower())
                redact_string(file, string_to_redact.upper())
                redact_string(file, string_to_redact.capitalize())
            except Exception as e:
                logging.error(f"{file} {string_to_redact} redaction failed {e}")
            else:
                logging.info(f"{file} {string_to_redact} redaction successful")

    logging.info("Script finished")


def import_csv():
    redact_set_import = set()
    try:
        with open(CSV_FILE, 'rt') as my_csv:
            reader = csv.reader(my_csv)
            student_list = list(reader)
            redact_set_import = set(itertools.chain(*student_list))
    except Exception as e:
        logging.warning(f"{e} CSV import failed. Is there a valid CSV named import.csv ?")
    return redact_set_import


def import_yaml():
    regex_dict = {}
    try:
        with open(YAML_FILE) as y:
            regex_dict = yaml.load(y, Loader=yaml.FullLoader)
    except Exception as e:
        logging.warning(f"{e} yaml import failed. Is there a valid yaml file named import.yaml ?")
    return regex_dict


def redact_regex(in_file, regex):
    """Redact matching regex from a html file"""
    filename_in = open(in_file, "r", encoding="utf8")
    filename_out = open("out.txt", "w+", encoding="utf8")
    content = filename_in.read()
    content_new = re.sub(regex, REDACT_REPLACE_WITH, content)
    filename_out.write(content_new)
    filename_in.close()
    filename_out.close()
    os.remove(in_file)
    os.rename("out.txt", in_file)


def redact_string(in_file, to_redact):
    """Redact matching string from a html file"""
    filename_in = open(in_file, "r", encoding="utf8")
    filename_out = open("out.txt", "w+", encoding="utf8")
    for line in filename_in:
        filename_out.write(line.replace(to_redact, REDACT_REPLACE_WITH))
    filename_in.close()
    filename_out.close()
    os.remove(in_file)
    os.rename("out.txt", in_file)


if __name__ == '__main__':
    '''argparse'''
    args = arguments()
    '''logging'''
    logfile = os.path.join(args.path, "log.txt")
    logging_format = '%(asctime)s:%(levelname)s:%(message)s'
    logging.basicConfig(filename=logfile, level=logging.DEBUG, format=logging_format)
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    logging.getLogger().addHandler(console)
    formatter = logging.Formatter(logging_format)
    console.setFormatter(formatter)
    '''main'''
    main(args.path)
