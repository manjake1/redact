This script will:

import strings from a csv and replace in all html files
(up to 100 columns)
This must be named "import.csv" and placed in the working directory.

import regexs from a yaml file and replace in all html files. This must be named "import.yaml" and placed in the working directory. 
There is a template included

Usage:

python main.py --path D:\temp\redact